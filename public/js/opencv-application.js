const SELECTORS = {
    opencvLoading: "opencv-loading",
    video: "video-input",
    canvas: "canvas-output",
};

const VIDEO_CONFIG = {
    mediaConstraints: {
        video: {
            fps: 60,
            width: 800,
            height: 600,
            facingMode: "environment",
        },
    },
};
const textResult = document.querySelector(".shape-result");
let reset = false;

const launchWebcam = async () => {
    try {
        const video = document.getElementById(SELECTORS.video);
        const userMedia = await navigator.mediaDevices.getUserMedia(VIDEO_CONFIG.mediaConstraints);
        VIDEO_CONFIG.webcamState = true;
        video.srcObject = userMedia;
        video.play();
        return video;
    } catch (err) {
        VIDEO_CONFIG.webcamState = false;
        console.log("An error occurred! " + err);
    }
};

const setupOpencvVideo = (video) => {
    const canvasVideo = document.getElementById(SELECTORS.video);
    canvasVideo.height = VIDEO_CONFIG.mediaConstraints.video.height;
    canvasVideo.width = VIDEO_CONFIG.mediaConstraints.video.width;

    return {
        src: new cv.Mat(canvasVideo.height, canvasVideo.width, cv.CV_8UC4),
        dstC1: new cv.Mat(canvasVideo.height, canvasVideo.width, cv.CV_8UC1),
        dstC3: new cv.Mat(canvasVideo.height, canvasVideo.width, cv.CV_8UC3),
        dstC4: new cv.Mat(canvasVideo.height, canvasVideo.width, cv.CV_8UC4),
        cap: new cv.VideoCapture(video),
    };
};
const contoursColor = [];
const randomScalar = () => {
    for (let i = 0; i < 10000; i++) {
        contoursColor.push([Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255), 0]);
    }
};

const largestContour = (contours) => {
    let indexContour = 0;
    for (let i = 0; i < contours.size(); ++i) {
        const contourSize = contours.get(i).matSize[0];
        if (indexContour < contourSize) {
            indexContour = i;
        }
    }
    return indexContour;
};

const processTemplate = (imgId, canvas, imgShow) => {
    const src = cv.imread(imgId);
    const dst = cv.Mat.zeros(canvas.height, canvas.width, cv.CV_8UC3);
    cv.cvtColor(src, src, cv.COLOR_RGBA2GRAY, 0);
    cv.threshold(src, src, 120, 255, cv.THRESH_BINARY);

    const contours = new cv.MatVector();
    const hierarchy = new cv.Mat();
    cv.findContours(src, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);

    const indexLargestContour = largestContour(contours);
    const contoursColor = new cv.Scalar(0, 0, 255);

    const returnedContour = contours.get(indexLargestContour);
    cv.drawContours(dst, contours, indexLargestContour, contoursColor, 3, cv.LINE_8, hierarchy, 100);

    const copySrc = src.clone();
    cv.imshow(imgShow, dst);

    src.delete();
    dst.delete();
    contours.delete();
    hierarchy.delete();

    return {
        img: copySrc,
        dimensions: [canvas.width, canvas.height],
        contour: returnedContour,
    };
};

const templateContours = async () => {
    const allTemplateContours = [];

    const monkeyCanvas = document.getElementById("monkey-template");
    const snakeCanvas = document.getElementById("snake-template");
    const rhinocerosCanvas = document.getElementById("rhinoceros-template");

    await loadImages(monkeyCanvas, "images/monkey.jpg");
    await loadImages(snakeCanvas, "images/snake.jpg");
    await loadImages(rhinocerosCanvas, "images/rhinoceros.jpg");

    const monkeyContour = processTemplate("monkey-template", monkeyCanvas, "monkey-result");
    const snakeContour = processTemplate("snake-template", snakeCanvas, "snake-result");
    const rhinocerosContour = processTemplate("rhinoceros-template", rhinocerosCanvas, "rhinoceros-result");

    return allTemplateContours.concat([monkeyContour, snakeContour, rhinocerosContour]);
};

const processVideo = (opencvStreaming, allTemplateContours) => {
    const begin = Date.now();

    if (!reset) {
        const src = opencvStreaming.src;
        const cap = opencvStreaming.cap;
        const dstC1 = opencvStreaming.dstC1;
        const dstC4 = opencvStreaming.dstC4;
        const cannyMat = cv.Mat.ones(5, 5, cv.CV_8U);
        const anchor = new cv.Point(-1, -1);
        const contours = new cv.MatVector();
        const hierarchy = new cv.Mat();
        cap.read(src);

        cv.cvtColor(src, dstC1, cv.COLOR_RGBA2GRAY, 0);
        cv.Canny(dstC1, dstC1, 50, 100, 3, false);

        cv.findContours(dstC1, contours, hierarchy, cv.RETR_CCOMP, cv.CHAIN_APPROX_SIMPLE);

        for (let i = 0; i < contours.size(); ++i) {
            const rect = cv.boundingRect(contours.get(i));
            const reactArea = rect.width * rect.height;
            if (reactArea > 100000 && rect.width * rect.height < 200000) {
                for (let indexContour = 0; indexContour < allTemplateContours.length; indexContour++) {
                    const template = allTemplateContours[indexContour];
                    const result = cv.matchShapes(template.contour, contours.get(i), 1, 0.0);
                    if (result < 0.05) {
                        reset = true;
                        let texture = new cv.Mat();
                        texture = src.roi(rect);
                        cv.imshow("texture-contour", texture);

                        const canvasTexture = document.getElementById("texture-contour");
                        const textureUrl = canvasTexture.toDataURL("image/png");
                        localStorage.setItem("texture", textureUrl);

                        switch (indexContour) {
                            case 0:
                                localStorage.setItem("scanResult", "monkey");
                                window.location.href = "/ar.html"
                                break;
                            case 1:
                                localStorage.setItem("scanResult", "snake");
                                window.location.href = "/ar.html"
                                break;
                            case 2:
                                localStorage.setItem("scanResult", "rhinoceros");
                                window.location.href = "/ar.html"
                                break;
                        }
                    }
                }
            }
        }
        cv.imshow("canvas-output", src);
    }
    const delay = 1000 / VIDEO_CONFIG.fps - (Date.now() - begin);
    setTimeout(processVideo, delay, opencvStreaming, allTemplateContours);
};

const loadImages = (canvas, pathImage) => {
    return new Promise((resolve) => {
        const context = canvas.getContext("2d");
        const templateImage = new Image();
        templateImage.src = pathImage;
        templateImage.onload = function () {
            canvas.width = templateImage.width;
            canvas.height = templateImage.height;
            context.drawImage(templateImage, 0, 0);
            resolve();
        };
    });
};

const resetVideo = () => {
    localStorage.removeItem("scanResult");
    localStorage.removeItem("texture");

    reset = false;
}

// eslint-disable-next-line no-unused-vars
const onOpenCvReady = () => {
    cv["onRuntimeInitialized"] = async () => {
        const video = await launchWebcam();
        const opencvStreaming = setupOpencvVideo(video);
        const allTemplateContours = await templateContours();

        randomScalar();
        setTimeout(processVideo, 0, opencvStreaming, allTemplateContours);

        document.querySelector(`.${SELECTORS.opencvLoading}`).classList.remove(SELECTORS.opencvLoading);
    };
};
