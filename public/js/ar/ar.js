const checkScan = () => {
    const scanResult = localStorage.getItem("scanResult");
    if (!scanResult) {
        redirect();
    }
};

const redirect = () => {
    window.location.href = "/scan.html";
};

const displayForm = () => {
    const shareForm = document.querySelector(".shareForm");
    shareForm.removeAttribute("hidden");
};

const closeForm = () => {
    const shareForm = document.querySelector(".shareForm");
    shareForm.setAttribute("hidden", true);
};

const submitForm = async () => {
    const form = {
        name: document.getElementById("name").value,
        email: document.getElementById("emailInput").value,
        active: true,
    };
    const post = document.getElementById("postInput").value;
    if (form.name && form.email) {
        // TODO remove this :| ^^ but we don't have time
        const res = await fetch("http://localhost:3000/api/v1/prospects", { method: "POST", body: form });
        console.log(res);
    }
    console.log("Empty form");
};

const load3DModel = () => {
    const scanResult = localStorage.getItem("scanResult");
    const aEntity = document.querySelector("a-entity");

    scanResult === "snake" || scanResult === "rhinoceros" ? aEntity.setAttribute("scale", `1 1 1`) : aEntity.setAttribute("scale", `2 2 2`);

    aEntity.setAttribute("obj-model", `obj: url(/models/${scanResult}.obj); mtl: url(/models/${scanResult}.mtl)`);
};

load3DModel();
