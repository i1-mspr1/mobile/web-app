const divInstall = document.getElementById('installContainer');
const butInstall = document.getElementById('butInstall');


/*Listen for the beforeinstallprompt event #
When the browser fires the beforeinstallprompt event, 
that's the indication that the Progressive Web App can be installed 
and an install button can be shown to the user. 
The beforeinstallprompt event is fired when the PWA meets the installability criteria.

--------------------------------------------------------
window.addEventListener('beforeinstallprompt', (event) => {
  // Prevent the mini-infobar from appearing on mobile.
  event.preventDefault();
  //console.log('👍', 'beforeinstallprompt', event);
  // Stash the event so it can be triggered later.
  window.deferredPrompt = event;
  // Remove the 'hidden' class from the install button container.
  divInstall.classList.toggle('hidden', false);
});


Handle the install button click 
To show the install prompt, call prompt() on the saved beforeinstallprompt event. 
Calling prompt() is done in the install button click handler because prompt() must be called from a user gesture.
---------------------------------------------------------------------------------------------------------------
*/

butInstall.addEventListener('click', async () => {
  //console.log('👍', 'butInstall-clicked');
  const promptEvent = window.deferredPrompt;
  if (!promptEvent) {
    // The deferred prompt isn't available.
    return;
  }
  // Show the install prompt.
  promptEvent.prompt();
  // Log the result
  //const result = await promptEvent.userChoice;
  //console.log('👍', 'userChoice', result);
  // Reset the deferred prompt variable, since
  // prompt() can only be called once.
  window.deferredPrompt = null;
  // Hide the install button.
  divInstall.classList.toggle('hidden', true);
});

/*


Track the install event #
Installing a Progressive Web App through an install button is only one way users can install a PWA. 

window.addEventListener('appinstalled', () => {
//console.log('👍', 'appinstalled', event);
  // Clear the deferredPrompt so it can be garbage collected
  window.deferredPrompt = null;
});




 * Warn the page must be served over HTTPS
 * The `beforeinstallprompt` event won't fire if the page is served over HTTP.
 * Installability requires a service worker with a fetch event handler, and
 * if the page isn't served over HTTPS, the service worker won't load.
*/
 if (window.location.protocol === 'http:') {
  const requireHTTPS = document.getElementById('requireHTTPS');
  const link = requireHTTPS.querySelector('a');
  link.href = window.location.href.replace('http://', 'https://');
  requireHTTPS.classList.remove('hidden');
  console.log(requireHTTPS);
}
else{
  console.log("protocol", window.location.protocol);

}

// Only register a service worker if it's supported 
if ("serviceWorker" in navigator) {
  window.addEventListener("load", function() {
    navigator.serviceWorker
      .register("../service-worker.js")
      .then(res => console.log("service worker registered", res))
      .catch(err => console.log("service worker not registered", err))
  })
}


