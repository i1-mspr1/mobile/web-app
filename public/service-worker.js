const CACHE_NAME = "sw-cache-example";
const toCache = [
    "/",
    "/home.html",
    "/ar.html",
    "/scan.html",
    "/js/status.js",
    "/js/opencv.js",
    "/js/opencv-application.js",
    "/js/ar/aframe-ar-nft.js",
    "/js/ar/aframe-gif-shader.min.js",
    "/js/ar/aframe-master.min.js",
    "/js/ar/ar.js",
    "/css/ar.css",
    "/css/bootstrap.min.css",
    "/css/header.css",
    "/css/style_home.css",
    "/css/style.css"
];

self.addEventListener("install", function (event) {
    event.waitUntil(
        caches
            .open(CACHE_NAME)
            .then(function (cache) {
                return cache.addAll(toCache);
            })
            .then(self.skipWaiting())
    );
});

self.addEventListener("fetch", function (event) {
    event.respondWith(
        fetch(event.request).catch(() => {
            return caches.open(CACHE_NAME).then((cache) => {
                return cache.match(event.request);
            });
        })
    );
});

self.addEventListener("activate", function (event) {
    event.waitUntil(
        caches
            .keys()
            .then((keyList) => {
                return Promise.all(
                    keyList.map((key) => {
                        if (key !== CACHE_NAME) {
                            console.log("[ServiceWorker] Removing old cache", key);
                            return caches.delete(key);
                        }
                    })
                );
            })
            .then(() => self.clients.claim())
    );
});
