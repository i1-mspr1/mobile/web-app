# Web App

The aim is to provide the visual to users so that they can scan the different drawings.

## Authors

| LASTNAME  | FIRSTNAME |
| --------- | --------- |
| DUHEM     | Matthieu  |
| JAHAN     | Robin     |
| RAOUL     | Antoine   |
| SAPIN     | Nicolas   |
| LECAPLAIN | Adrien    |

## Project setup

### Windows

```bash
npm install -g win-node-env
```

Before all, add `.env` to run it locally at the root of the project

Then install the project with

```
npm install
```

## Definition of .env variables

| VARIABLE      | TYPE   | DEFAULT        | DESCRIPTION                                |
| ------------- | ------ | -------------- | ------------------------------------------ |
| PUBLIC_FOLDER | string | ../public      | Path to the folder and static files        |
| HTTP_PORT     | number | 8080           | Sets the port for the http protocol        |
| HTTPS_PORT    | number | 8001           | Sets the port for the https protocol (SSL) |
| SSL_KEY_PATH  | string |                | Path to the SSL key                        |
| SSL_CERT_PATH | string |                | Path to the SSL cert                       |
| API_URL       | string | http://localhost:3000 | URL to the API                             |

## Compiles and hot-reloads for local

```
npm run watch:local
```

## Lints and fixes files

```
npm run lint
```

## Security checking

```
npm run security:audit
```

## Deployement

To deploy the application and test the pwa and camera on a deployment environment with an SSL certificate, run the following commands in order

```bash
npm run prod                 # Run the server within http for ngrok
ngrok http ${listening_port} # Listening on the specificied port
```
