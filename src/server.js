require('dotenv').config();
const express = require("express");
const path = require("path");
const fs = require("fs");
const https = require("https");

const httpPort = process.env.HTTP_PORT;
const httpsPort = process.env.HTTPS_PORT;

// Self-signed certificate to test the pwa
const key = fs.readFileSync(process.env.SSL_KEY_PATH);
const cert = fs.readFileSync(process.env.SSL_CERT_PATH);

const app = express();
console.log(path.join(__dirname, process.env.PUBLIC_FOLDER));
app.use(express.static(path.join(__dirname, process.env.PUBLIC_FOLDER)));

if (process.env.NODE_ENV === "local") {
    const server = https.createServer({ key: key, cert: cert }, app);

    app.use((req, res, next) => {
        if (!req.secure) {
            return res.redirect("https://" + req.headers.host + req.url);
        }
        next();
    });
    server.listen(httpsPort, function () {
        console.log(`Listening on port ${httpsPort}!`);
    });
}


app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname, `${process.env.PUBLIC_FOLDER}/home.html`));
});

app.listen(httpPort, function () {
    console.log(`Listening on port ${httpPort}!`);
});
